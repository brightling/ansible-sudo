SUDO
====

Role to fully manage [sudo](http://www.sudo.ws) and sudoers configuration.  
This role tries to be easy to configure while also giving all the configuration available with sudo.

Requirements
------------

Only currently tested for Debian Jessie.  
Requires Ansible 2.0+.

Role Variables
--------------

Variables to configure the sudo role and sudoers configuration.

### Main Role Variables
Variables which are used to manage to role.

* `sudo_state`:  The action for the role to take.
  * _present_ (default): Install and configure sudo on the client.
  * _absent_: Uninstall sudo and remove configuration files from the client.

### Main Sudoer Configuration
Variables which can be used to configure the main sudoers configuration file.
For full information on each sudoers option see [sudoers documentation](https://www.sudo.ws/man/sudoers.man.html).

#### Sudoers Generic Defaults
Set the generic sudoers defaults with a key:val dictionary. Where the key is the defaults option you which to set.  
The value can be a boolean, int, string or list; consult the sudoers documentation for the correct types for each option.
To add to and/or remove from a list option use a dictionary with the keys **append** and **remove**.

* `sudo_sudoers_defaults`: 

  **Main Default:**
  ```yaml
    _sudo_sudoers_default: 
      env_reset: true  
      mail_badpass: true  
      secure_path: ['/usr/local/sbin', '/usr/local/bin', '/usr/sbin', '/usr/bin', '/sbin', '/bin']  
  ```

***secure_path*** is a specical option which can be a list, and is joined into a string.

#### Sudoers User/Host/Runas/Cmnd Specific Defaults
Set specific defaults for users, hosts, runas or commands using A Dictionary of Dictionaries where the outer dictionary is the target and 
the inner dictionary is the default options; uses the same format as `sudo_sudoers_defaults`.  

* `sudo_sudoers_user_defaults`: Default _{}_.
* `sudo_sudoers_runas_defaults`: Default _{}_.
* `sudo_sudoers_host_defaults`: Default _{}_.
* `sudo_sudoers_cmnd_defaults`: Default _{}_.

#### Sudoers Aliases
Set sudoers aliases. Aliases are sorted dictionaries where the key is the alias name and the value is a string or list.

* `sudo_sudoers_user_aliases`: Default _{}_, Dictionary to set sudoers user aliases.
* `sudo_sudoers_runas_aliases`: Default _{}_, Dictionary to set sudoers runas aliases.
* `sudo_sudoers_host_aliases`: Default _{}_, Dictionary to set host aliases.
* `sudo_sudoers_cmnd_aliases`: Default _{}_, Dictionary to set cmnd aliases.

#### Sudoers Specification
Set the sudoers control specification with a list of dictionary options as follows.

* `sudo_sudoers_spec`: 
  * `user`: _required_. The name of the user to apply the specification.
  * `hosts`: Default _ALL_. Takes a single string or a list.
  * `runas_user`: Default _null_. Takes a single string or a list.
  * `runas_group`: Default _null_. Takes a single string or a list.
  * `tags`: Defaults _null_. Takes a single string or a list.
  * `commands`: Default _ALL_. Takes a single string or a list.

  **Main Default:**
  ```yaml
    _sudo_sudoers_spec:
      - user: root
      - user: %wheel
  ```
  **Debian Default:**
  ```yaml
    _sudo_sudoers_spec:
      - user: root
      - user: %sudo
  ```
  **Ubuntu Natty & Ubuntu Oneiric Default:**
  ```yaml
    _sudo_sudoers_spec:
      - user: root
      - user: %admin
  ```

Limitations: 
* Currently it is only possible to define 1 user, for multiple users use an User Alias. 
* It is only possible to define 1 set of speicifications per config line, use multiple lines for multiple definitions.

#### Sudoers Configuration Directory
Sudoers can be configured using separate files in a directory, /etc/sudoers.d by default. 
Configuration is the same as the global sudoers file just without the global default options available.

* `sudoers_configs`: Default _{}_, a Dictionary of Dictionaries. The key is the filename to use. Valid dictionaries:
  * `defaults`: List of Dictionaries. see sudoers_defaults.
  * `user_aliases`: Dictionary, see sudoers_user_aliases.
  * `runas_aliases`: Dictionary, see sudoers_runas_aliases.
  * `host_aliases`: Dictionary, see sudoers_host_aliases.
  * `cmnd_aliases`: Dictionary, see sudoers_cmnd_aliases.
  * `users`: List of Dictionaries, see sudoers_users.

### Additional Variables
Less commonly used variables which are available.

* `sudo_packages`: Default _[sudo]_, List of packages to install.
* `sudo_sudoers_directory`: Default _/etc/sudoers.d_.


Dependencies
------------

None

Example Playbook
----------------

Install and configure with the default Debian configuration. 
Allows root and group default sudo to run all commands as all users.
```jinja
- hosts: all
  roles:
     - brightling.sudo
```
Install sudo and replicate configuration as defined in the sudoers documentation.

```jinja
- hosts: all
  roles:
    - { role: brightling.sudo,
        sudo_sudoers_defaults: {
          syslog: 'auth',
          env_keep: {'append': ['DISPLAY', 'HOME']},
        },
        sudo_sudoers_user_defaults: {
          millert: {
            authenticate: false
          },
          FULLTIMERS: {
            lecture: false
          }
        },
        sudo_sudoers_host_defaults: {
          SERVERS: {
            log_year: true,
            logfile: '/var/log/sudo.log'
          }
        },
        sudo_sudoers_runas_defaults: {
          root: {
            set_logname: false
          }
        },
        sudo_sudoers_cmnd_defaults: {
          PAGERS: {
            noexec: true
          }
        },
        sudo_sudoers_user_aliases: {
          fulltimers: ['millert', 'mikef', 'dowdy'],
          parttimers: ['bostley', 'jwfox', 'crawl'],
          webmasters: ['will', 'wendy', 'wim'],
         },
        sudo_sudoers_runas_aliases: {
          op: ['root', 'operator'],
          db: ['orcale', 'sybase'],
          admingrp: ['adm', 'oper'],
        },
        sudo_sudoers_host_aliases: {
          sparc: ['bigtime', 'eclipse', 'moet', 'archor'],
          sgi: ['grolsch', 'dandelion', 'black'],
          alpha: ['widget', 'thalamus', 'foobar'],
          hppa: ['boa', 'nag', 'python'],
          cunets: '128.138.0.0/255.255.0.0',
          csnets: ['128.138.243.0', '128.138.204.0/24', '128.138.242.0'],
          servers: ['master', 'mail', 'www', 'ns'],
          cdrom: ['orion', 'perseus', 'hercules']
        },
        sudo_sudoers_cmnd_aliases: {
          dumps: ['/usr/bin/mt', '/usr/sbin/dump', '/usr/sbin/rdump', 
            '/usr/sbin/restore', '/usr/sbin/rrestore',
            'sha224:0GomF8mNN3wlDt1HD9XldjJ3SNgpFdbjO1+NsQ== /home.operator/bin/start_backups'],
          kill: '/usr/bin/kill',
          printing: ['/usr/sbin/lpc', '/usr/bin/lprm'],
          shutdown: '/usr/sbin/shutdown',
          halt: '/usr/sbin/halt',
          reboot: '/usr/sbin/reboot',
          shells: ['/usr/bin/sh', '/usr/bin/csh', '/usr/bin/ksh', '/usr/local/bin/tcsh',
            '/usr/bin/rsh', '/usr/local/bin/zsh'],
          su: '/usr/bin/su',
          pagers: ['/usr/bin/more', '/usr/bin/pg', '/usr/bin/less']
        },
        sudo_sudoers_spec: [
          {user: 'root'},
          {user: '%wheel'},
          {user: 'FULLTIMERS', tags: 'NOPASSWD'},
          {user: 'PARTTIMERS', },
          {user: 'jack', hosts: 'CSNETS'},
          {user: 'lisa', hosts: 'CUNETS'},
          {user: 'operator', commands: ['DUMPS', 'KILL', 'SHUTDOWN', 'HALT', 'REBOOT', 
            'PRINTING', 'sudoedit /etc/printcap', '/usr/oper/bin/']},
          {user: 'joe', commands: '/usr/bin/su operator'},
          {user: 'pete', hosts: 'HPPA', commands: ['/usr/bin/passwd [A-Za-z]*', '!/usr/bin/passwd root']},
          {user: '%opers', runas_groups: 'ADMINGRP', commands: '/usr/sbin/'},
          {user: 'bob', hosts: 'SPARC', runas_users: 'OP'},
          {user: 'bob', hosts: 'SGI', runas_users: 'OP'},
          {user: 'jim', hosts: '+biglab'},
          {user: '+secretaries', commands: ['PRINTING', '/usr/bin/adduser', '/usr/bin/rmuser']}
          {user: 'fred', runas_users: 'DB', tags: 'NOPASSWD'},
          {user: 'john', hosts: 'ALPHA', commands: ['/usr/bin/su [!-]*', '!/usr/bin/su *root*']},
          {user: 'jen', hosts: ['ALL', '!SERVERS']},
          {user: 'jill', hosts: 'SERVERS', commands: ['/usr/bin/', '!SU', '!SHELLS']},
          {user: 'steve', hosts: 'CSNETS', runas_users: 'operator', commands: '/usr/local/op_commands/'},
          {user: 'matt', hosts: 'valkyrie', commands: 'KILL'},
          {user: 'WEBMASTERS', hosts: 'www', runas_users: 'www'},
          {user: 'WEBMASTERS', hosts: 'www', runas_users: 'root', commands: '/usr/bin/su www'},
          {user: 'ALL', hosts: 'CDROM', tags: 'NOPASSWD', commands: ['/sbin/umount /CDROM', '/sbin/mount -o nosuid\,nodev /dev/cd0a /CDROM']},
        ],
      }
```

Example which configures sudoers using the sudoers.d directory. 

```jinja
- hosts: all
  roles:
    - { role: brightling.sudo,
        sudo_sudoersd_configs: {
          networks: {
            user_aliases: {
              network_admins: ['phill', 'sophie', 'paul'],
              network_ops: 'sam'
            },
            command_aliases: {
              network_admin: 'sudoedit /etc/sysconfig/network-scripts/*',
              network_ops: ['/sbin/ifup', '/sbin/ifdown']
            },
            spec: [
              {user: 'NETWORK_ADMINS', commands: ['NETWORK_ADMIN', 'NETWORK_OPS'] },
              {user: 'NETWORK_OPS', commands: ['NETWORK_OPS'] }
            ]   
          },  
          admins: {
            user_aliases: {
              admins: ['jack', 'phill', 'jenny']
            },  
            user_defaults: {
              ADMINS: {
                lecture: false,
                passwd_tries: 2
              }   
            },
            spec: [
              {user: 'ADMINS'}
            ]
          }  
        }
      }
```


Remove sudo from a machine and its configuration.

```jinja
- hosts: all
  roles:
    - { role: brightling.sudo,
        sudo_state: absent }
```


License
-------

GPLv3

Author Information
------------------

- Robert White | [e-mail](mailto:sentryveil00@gmail.com) | [GitLab](https://gitlab.com/brightling)
